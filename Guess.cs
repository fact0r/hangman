using System.Collections.Generic;
using System.Linq;

namespace hangman
{
    class Guess
    {
        public char[] guess;
        public readonly int length;
        public List<char> wrongLetters;

        public Guess(int length)
        {
            guess = Enumerable.Repeat('*', length).ToArray();
            this.length = length;
            wrongLetters = new List<char>();
        }

        public void AddLetter(char letter, int index)
        {
            guess[index] = letter;
        }

        public void AddWrongLetter(char letter)
        {
            wrongLetters.Add(letter);
        }

        public bool IsPossibleWord(string word)
        {
            return IsMatch(word) && IsCorrectLength(word) && ExcludesWrongLetters(word);
        }

        public string GetState()
        {
            return new string(guess)+ '\n' + new string(wrongLetters.ToArray()) ;
        }

        private bool IsMatch(string word)
        {
            return guess
                .Zip(word, (l1,l2) => IsLetterMatch(l1,l2))     //Make sure all letters match
                .Aggregate(true, (acc, x) => acc && x);         //Reductive And
        }

        private bool IsLetterMatch(char l1, char l2)
        {
            // * serves as wildcard in implementation
            return l1 == '*' || l2 == '*' || l1 == l2;
        }

        private bool ExcludesWrongLetters(string word)
        {
            bool b = true;
            foreach (char c in wrongLetters)
                b &= !word.Contains(c);
            return b;
        }


        private bool IsCorrectLength(string word)
        {
            return word.Length == length;
        }


    }
}
