using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace hangman
{
    class WordList
    {
        public readonly string[] words;

        public WordList()
        {
            StreamReader sr = new StreamReader("dict.txt");
            List<string> lWords = new List<string>();
            string word;

            while ((word = sr.ReadLine()) != null)
            {
            word = word.Split(' ')[0];
            lWords.Add(word);
            }
            words = lWords.ToArray();

        }

        public WordList(string[] ws)
        {
            words = ws;
        }

        public WordList Where(Func<string, bool> f)
        {
            var ws = words.Where(f).ToArray();
            return new WordList(ws);
        }

        public Dictionary<char, int> CountLetters()
        {
            Dictionary<char, int> d = new Dictionary<char, int>();

            // merge all words to list of chars
            var letters = words.Aggregate((acc,x) => acc + x).ToArray();

            // count all letters
            foreach (char l in letters)
                if (!d.ContainsKey(l))
                    d.Add(l, 1);
                else
                    d[l]++;

            // sort dicitionary by descending value -> most common letter first
            d = (from entry in d orderby entry.Value descending select entry).ToDictionary(pair => pair.Key, pair => pair.Value);

            return d;
        }

    }
}
