﻿using System;

namespace hangman
{
    class Program
    {
        static void Main(string[] args)
        {
            Hangman hangman = new Hangman();
            Guess guess = new Guess(7);

            hangman.CreateGuess();

            while(true)
            {
                hangman.Loop();
            }

        }

        private static void PrintDict(WordList dict)
        {
            foreach (string word in dict.words)
                Console.WriteLine(word);
        }
    }
}
