using System;
using System.Collections.Generic;
using System.Linq;

namespace hangman
{
    class Hangman
    {
        private WordList wordlist;
        private Guess guess;

        public Hangman()
        {
            wordlist = new WordList();
        }

        public void Loop()
        {
            if (wordlist.words.Length <= 10 )
            {
                foreach(string word in wordlist.words)
                    Console.WriteLine(word);
            }
            UpdateGuess();
            UpdateWordList();
            PrintState();
        }

        public void CreateGuess()
        {
            int length = GetLetter("How long is your word?") - '0';
            guess = new Guess(length);
        }


        private void UpdateGuess()
        {
            char letter = GetLetter("A letter that is in the word");
            int index = GetLetter("Where the letter is (0 indexed)") - '0';
            var wrongLetter = GetLetter("An incorrect letter");

            if(Char.IsLetter(letter) && index <= guess.length )
            {
                guess.AddLetter(letter, index);
            }
            if(Char.IsLetter(wrongLetter))
            {
                guess.AddWrongLetter(wrongLetter);
            }
        }

        private void UpdateWordList()
        {
            wordlist = wordlist.Where(guess.IsPossibleWord);
        }

        private void PrintState()
        {
            string guessState = guess.GetState();
            Dictionary<char, int> commonLetters = wordlist.CountLetters();

            Console.WriteLine(guessState);

            int i = 0;
            foreach (var l in commonLetters)
            {
                if(guess.wrongLetters.Contains(l.Key) || guess.guess.ToList().Contains(l.Key))
                    continue;
                i++;
                Console.WriteLine(l.Key);
                if(i >=5)
                {
                    break;
                }
            }
        }

        private static char GetLetter(string text)
        {
            Console.Write("{0} : ", text);
            return GetLetter();
        }
        private static char GetLetter()
        {
            var c = Console.ReadKey().KeyChar;
            Console.WriteLine();
            return c;

        }

    }
}
